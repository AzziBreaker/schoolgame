package classes;

import java.io.Serializable;

public abstract class Attack implements Serializable{

    public Attack(String name, int energyCost, int strenght) {
       
        this.name = name;
        this.energyCost = energyCost;
        this.strenght = strenght;
    }
    
    public String name;

    public int energyCost;

    public int strenght;
}
