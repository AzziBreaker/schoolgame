package classes;

import java.io.Serializable;

public class Room implements Serializable{
    public Room( char[][] arr) {
        char[][] layout = arr;
        String[][] paths = new String[Size][Size];
        this.layout = layout;
        this.paths = paths;
        this.CreateImg();
        
    }

    public Room() {
        
    }
    
    public char[][] layout;

    public String[][] paths;

    public int Size = 7;
    public int Id;

    public boolean IsItemRoom = false;
    public boolean IsRoomMade = false;
    
    private void CreateImg() {
        for(int i = 0;i<Size;i++)
        {
            for(int j = 0;j<Size;j++)
            {
                paths[i][j] = "";

            }

            
        }
    }


    public void Print() {
            for(int i = 0;i<Size;i++)
            {
                for(int j = 0;j<Size;j++)
                {
                    
                    if(j!=Size)
                    {
                        System.out.print(layout[i][j]+" ");
                    }
                    else
                    {
                        System.out.print(layout[i][j]);
                    }


                }

                System.out.println();
            }


    }
    
}
