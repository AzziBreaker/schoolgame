package classes;

public class ItemArmor extends Item {
    public ItemArmor(String type, String set, int armor) {
        this.type = type;
        this.set = set;
        this.armor = armor;
    }

    public int armor;
}
