package classes;

import java.io.Serializable;

public class Item implements Serializable{
    public String type;
    public String set;
    public String rarity;
}
