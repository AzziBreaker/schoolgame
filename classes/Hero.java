package classes;

import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

import frame.GameFrame;

public class Hero implements Serializable{

    public Hero(String name, String gender) {


        this.name = name;
        this.gender = gender.toLowerCase().charAt(0);
        this.UpdateExperience();
        this.currentHp = maxHp;

        AttackDamaging slash = new AttackDamaging("Punch", 0, 1);
        AttackDamaging stab = new AttackDamaging("Slice", 20, 2);
        AttackHealing heal = new AttackHealing("Holy Water", 10, 1);
        AttackHealing healOverTime = new AttackHealing("Holier Water", 50, 2);
        Attack[] attacks = {slash,stab,heal,healOverTime};

        for (Attack attack : attacks) {
            this.Attacks.add(attack);
        }

       
    }

    public Hero(Scanner scanner) {

        Introduction(scanner);
        
    }

    public ItemWeapon weapon = null;
    public ItemArmor helmet;
    public ItemArmor boots;
    public ItemArmor leggings;
    public ItemArmor chestplate;
    public List<Attack> Attacks = new ArrayList<Attack>();
    public String name;
    public String pronounIs;
    public String pronounHas;
    public String pronounRefer;
    public int level = 1;
    public int experience = 0;
    public int currentHp;
    public char gender;
    public int strength = 1;
    public int armor = 0;
    public int age;
    public int RoomPositionX = 3;
    public int RoomPositionY = 3;
    public int RoomX = 1;
    public int RoomY = 1;
    public int whichFloor = 1;
    private int floorSize = 7;
    private int maxHp;

    private boolean canIMove(char whereGo, char[] cantGo, Room room, Map floorOneMap, Hero hero) {
        char canGo = '?';
        if (whereGo == 'a') {


            if (this.RoomPositionY - 1 == -1) {
                return true;
            } else {

                canGo = room.layout[this.RoomPositionX][this.RoomPositionY - 1];


            }
        } else if (whereGo == 'd') {

            if (this.RoomPositionY + 1 == floorSize) {
                return true;
            } else {

                canGo = room.layout[this.RoomPositionX][this.RoomPositionY + 1];


            }
        } else if (whereGo == 'w') {
            if (this.RoomPositionX - 1 == -1) {
                return true;
            } else {
                canGo = room.layout[this.RoomPositionX - 1][this.RoomPositionY];


            }
        } else if (whereGo == 's') {
            if (this.RoomPositionX + 1 == floorSize) {
                return true;
            } else {

                canGo = room.layout[this.RoomPositionX + 1][this.RoomPositionY];


            }
        }
        
        
        for (char c: cantGo) {

            if (canGo == c) {
                return false;
            }


        }
        if (canGo == 'e') {
            Enemy enemy = floorOneMap.GetEnemy();
            while (enemy.IsAlive == false) {
                enemy = floorOneMap.GetEnemy();
            }
            
            Fight.Start(enemy,hero);
            return true;
        }
        //TODO: fix chest and make chest class
        if (canGo == 'c') {
            return true;
        }
        
        return true;
    }

    public void Move(char direction, Map map) {

        Room room = map.GetRoom();
        room.layout[this.RoomPositionX][this.RoomPositionY] = ' ';
        char[] cantGo = {
            'o', '─', '│', 'c','┌','┐','└', '┘'
        };
        boolean canIMove = canIMove(direction, cantGo, room, map, this);


        if (canIMove) {
            if (direction == 'a') {


                if (this.RoomPositionY - 1 == -1) {
                    this.RoomPositionY = floorSize-1;
                    this.RoomY--;
                } else {

                    this.RoomPositionY--;


                }
            } else if (direction == 'd') {

                if (this.RoomPositionY + 1 == floorSize) {
                    this.RoomPositionY = 0;
                    this.RoomY++;
                } else {
                    this.RoomPositionY++;
                }
            } else if (direction == 'w') {
                if (this.RoomPositionX - 1 == -1) {
                    this.RoomPositionX = floorSize-1;
                    this.RoomX--;
                } else {
                    this.RoomPositionX--;
                }
            } else if (direction == 's') {
                if (this.RoomPositionX + 1 == floorSize) {
                    this.RoomPositionX = 0;
                    this.RoomX++;
                } else {
                    this.RoomPositionX++;
                }
            }
        }
    }
    
    public void MoveWithMouse(Map map, int x, int y) {
        Room room = map.GetRoom();
        char[] cantGo = {
            //TODO: fix enemy interaction
            'o', '─', '│', 'c','┌','┐','└', '┘'
        };

        boolean canIMove = true;
        for (char c : cantGo) {
            if (room.layout[x][y] == c){
                canIMove = false;
            }
        }

        if (canIMove) {
            map.GetRoom().layout[x][y] = ' ';
            this.RoomPositionX = x;
            this.RoomPositionY = y;
            
        }
    }

    public void MoveBetweenRoom(char direction,Map map) {
        switch (direction) {
            case 'a':
                this.RoomY--;
                this.RoomPositionY=map.GetRoom().Size;
                break;
            case 'd':
                this.RoomY++;
                this.RoomPositionY=0;
                break;
            case 'w':
                this.RoomX--;
                this.RoomPositionX=map.GetRoom().Size;
                break;
            case 's':
                this.RoomX++;
                this.RoomPositionX=0;
                break;
            default:
                break;
        }
    }
    public void MoveBetweenRoom() {
        
    }
    public void UpdateExperience() {
        int carriedXp = 0;

        if (experience >= 10) {
            carriedXp = experience % 10;
            experience = carriedXp;
            level++;
            this.currentHp = 7 + 3 * level;
            System.out.println("Level up!");
            this.strength++;
            
        }

        
        this.maxHp = 7 + 3 * level;


    }

    //TODO
    public void Stats() {
        System.out.format("%s's stats\n", this.name);
        System.out.format("~~~~~~~~~~~~~~~~~\n");
        System.out.format("Strength %d\n", this.strength);
        System.out.format("Armor %d\n", this.armor);
        System.out.format("Level %d with %d experience\n", this.level, this.experience);
        System.out.format("Hp: %d / %d\n", this.currentHp, this.maxHp);
        if(this.weapon != null){
            System.out.format("Weapon: %s %s\n", weapon.set, weapon.type);
        } else{
            System.out.format(("No weapon!\n"));
        }   
        if(this.helmet!=null){
            //System.out.format("Helmet: %s's %s", helmet.set, helmet.type);
            System.out.format("Helmet: %s %s\n", helmet.set, helmet.type);
        } else{
            System.out.format(("No weapon!\n"));
        }
        if (this.chestplate!=null) {
            //System.out.format("Chestplate: %s's %s", chestplate.set, chestplate.type);
            System.out.format("Chestplate: %s %s\n", chestplate.set, chestplate.type);
        } else {
            System.out.format(("No weapon!\n"));
        }
        if (this.leggings!=null) {
            //System.out.format("Leggings: %s's %s", leggings.set, leggings.type);
            System.out.format("Leggings: %s %s\n", leggings.set, leggings.type);
        } else {
            System.out.format(("No weapon!\n"));
        }
        if (this.boots!=null) {
            //System.out.format("Boots: %s's %s", boots.set, boots.type);
            System.out.format("Boots: %s %s\n", boots.set, boots.type);
        } else {
            System.out.format(("No weapon!\n"));
        } 
        System.out.format("~~~~~~~~~~~~~~~~~~\n");
    }

    public void Introduction(Scanner scanner) {

        System.out.println("What's their name?");
        this.name = scanner.nextLine();
        System.out.format("What gender is %s?\n", name);
        this.gender = scanner.nextLine().toLowerCase().charAt(0);
        while (gender != 'w' && gender!= 'm' && gender!= 'f') {
            System.out.format("Not a valid gender for %s!\n", name);
            gender = scanner.nextLine().toLowerCase().charAt(0);
        }

        if (this.gender == 'm') {
            pronounHas = "his";
            pronounIs = "he";
            pronounRefer = "him";
        } else if (this.gender == 'f' || this.gender == 'w') {
            pronounHas = "hers";
            pronounIs = "she";
            pronounRefer = "her";
        }
        
        System.out.format("How old is %s?\n", this.pronounIs);
        this.age = scanner.nextInt();
        this.UpdateExperience();
        this.currentHp = maxHp;
        scanner.nextLine();

    }

    public void Heal(int heal) {
        this.currentHp+=heal;
        if (currentHp>maxHp) {
            currentHp = maxHp;
        }
    }

    public void EquipWeapon(ItemWeapon weapon) {
        this.strength+=weapon.damage;
        this.weapon = weapon;
    }

    public ItemWeapon UnequipWeapon() {
        this.strength-=this.weapon.damage;
        ItemWeapon droppedWeapon = this.weapon;
        return droppedWeapon;
    }

    public void EquipArmor(ItemArmor armorPiece) {
        this.armor+=armorPiece.armor;
        if (armorPiece.type.equals("Helmet")) {
            this.helmet = armorPiece;
        } else if (armorPiece.type.equals("Chestplate")) {
            this.chestplate = armorPiece;
        } else if (armorPiece.type.equals("Leggings")) {
            this.leggings = armorPiece;
        } else if (armorPiece.type.equals("Boots")) {
            this.boots = armorPiece;
        }
    }
//TODO: unequip
    public static void ShowStats() {
        GameFrame.ShowStats();
    }
}