package classes;

public class ItemWeapon extends Item {
    public ItemWeapon(String type, String set, int damage) {
        this.type = type;
        this.set = set;
        this.damage = damage;
    }

    public int damage;
}
