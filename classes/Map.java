package classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import frame.GameFrame;

public class Map implements Serializable {

    public Map(int id, Hero hero) {
        this.hero = hero;
        Room[][] rooms = new Room[3][3];
        
        this.Rooms = rooms;
        this.Chapter = id;

        AttackDamaging slash = new AttackDamaging("Slash", 10, 1);
        AttackDamaging stab = new AttackDamaging("Stab", 20, 2);
        Attack[] attacks = {slash,stab};

        for (Attack attack : attacks) {
            this.Attacks.add(attack);
        }

        Enemy dani = new Enemy("Dani", 8, 1, this.Attacks);
        Enemy iskren = new Enemy("Iskren", 1, 1, this.Attacks);
        Enemy eftim = new Enemy("Efto", 3,1, this.Attacks);
        Enemy niki = new Enemy("Niki", 22, 1, this.Attacks);
        Enemy maxim = new Enemy("Maxim", 3, 1, this.Attacks);

        Enemy[] enemies = {dani,iskren,eftim,niki,maxim};
        
        for (Enemy enemy : enemies) {
            this.Enemies.add(enemy);
        }

    }

    public GameFrame frame;
    public int Chapter;
    transient public int spawnCounter = 0;
    public Hero hero;
    //private int EnemiesCount;
    public boolean itemRoomSpawned = false;
    public Room[][] Rooms;
    public List<Enemy> Enemies = new ArrayList<Enemy>();
    public List<Attack> Attacks = new ArrayList<Attack>();

    public void ShowRoom() {  
        
        Rooms[hero.RoomX][hero.RoomY].layout[hero.RoomPositionX][hero.RoomPositionY] = 'x';
        Rooms[hero.RoomX][hero.RoomY].Print();
    }

    public Room GetRoom() {  
        
        return Rooms[hero.RoomX][hero.RoomY];
        
    }

    public Enemy GetEnemy(){

        
        int maximum = Enemies.size();
        int minimum = 0;
        int enemyNum = (int)(Math.random()*(maximum - minimum)) + minimum;
        if (!Enemies.get(enemyNum).IsAlive) {
            
        }
        return Enemies.get(enemyNum);

    }
    
    private int SpawnEnemy(){

        int maximum = 100;
        int minimum = 1;
        int spawnNum = (int)(Math.random()*(maximum - minimum)) + minimum;
        return spawnNum;

    }

    private int SpawnItemRoom(){

        int maximum = 9;
        int minimum = 1;
        int spawnNum = (int)(Math.random()*(maximum - minimum)) + minimum;
      
            if (spawnNum==(((this.Rooms.length*this.Rooms.length)-1)/2)+1) {
                return SpawnItemRoom();
            }
        
        return spawnNum;

    }

    public void makeMap() {
        int counter = 1;
        int location = this.SpawnItemRoom();
        

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                char[][] currentTemplate = {
                    {
                        '┌',
                        '─',
                        '─',
                        ' ',
                        '─',
                        '─',
                        '┐'
                    },
                    {
                        '│',
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        '│'
                    },
                    {
                        '│',
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        '│'
                    },
                    {
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        ' '
                    },
                    {
                        '│',
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        '│'
                    },
                    {
                        '│',
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        ' ',
                        '│'
                    },
                    {
                        '└',
                        '─',
                        '─',
                        ' ',
                        '─',
                        '─',
                        '┘'
                    },
                };
                
                
               
               if(spawnCounter<Enemies.size())
                for (int k = 0; k < currentTemplate.length; k++) {
                   for (int p = 0; p < currentTemplate.length; p++) {
                       
                        char c = currentTemplate[k][p];
                        //TODO: FIX THIS
                        if (SpawnEnemy()<=4 && c == ' ' && (k!=0 || k!= 4 || p!= 0 || p!= 4) && (counter != location)) {
                            
                          
                          
                            currentTemplate[k][p] = 'e';
                            spawnCounter++;
                            System.out.println("Spawned enemy "+ spawnCounter);
                            
                            
                           
                        }
                   }
               
               
            }
                boolean didItSpawn = false;
                if (counter==location) {
                    currentTemplate[3][3] = 'c';
                    didItSpawn = true;
                }

                Room newRoom = new Room(currentTemplate);
                newRoom.Id = counter;
                counter++;
                newRoom.IsItemRoom = didItSpawn;
                

                if (i == 0) {
                    newRoom.layout[0][3] = '─';
                }
                if (j == 0) {
                    newRoom.layout[3][0] = '│';
                }
                if (i == 2) {
                    newRoom.layout[6][3] = '─';
                }
                if (j == 2) {
                    newRoom.layout[3][6] = '│';
                }

               
                this.Rooms[i][j] = newRoom;
            }

        }
    }

    public void whoLeft() {

        System.out.println("Left enemies are:");

        for (Enemy enemy  : Enemies) {
            if (enemy.IsAlive) {
                System.out.format(" %s", enemy.name);
            }
            
        }
    }
}
