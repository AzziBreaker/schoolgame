package classes;
import java.io.Serializable;
import java.util.List;

public class Enemy implements Serializable{
    public Enemy(String name, int hp, int strenght, List<Attack> attacks) {
        this.name = name;
        this.hp = hp;
        this.strenght = strenght;
        this.Attacks = attacks;
    }

    public String name;
    transient public boolean IsAlive = true;
    public int strenght;
    public int hp;
    public int Energy;
    public List<Attack> Attacks;
    
}
