package classes;

import java.io.Serializable;
import java.util.Scanner;

public class Fight implements Serializable{

    public Fight() {
    }

    public boolean isHappening = true;

    public static boolean Start(Enemy enemy, Hero hero)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.format("Do u want to fight %s\n",enemy.name);
        
        //TODO:Make fight work!
        //Dialog d = new Dialog();
        boolean startFight = scanner.nextLine().equals("yes") ? true:false;
        
        if(startFight)
        {
            System.out.format("%s decided to fight %s\n", hero.name,enemy.name);
            return Battle(scanner, enemy, hero);
        }
        else
        {
            
            System.out.format("%s is scared of %s\n", hero.name,enemy.name);
            return false;
        
        }
    }

    private static boolean Battle(Scanner scanner, Enemy enemy, Hero hero) {
        int carriedHp = 0;
        while (enemy.hp>0 && hero.currentHp>0) {
            String command = scanner.next().toLowerCase();
            
            int dmgRed = 0;
            if(command.equals("attack"))
            {
                String attack = scanner.next().toLowerCase();
                Attack usedAttack;
                
                int supposedHeal = 0;

                if (attack.equals("one")) {
                    usedAttack = hero.Attacks.get(0);
                    enemy.hp-=hero.strength;
                    System.out.format("%s dealt %d damage!\n", hero.name, 1*hero.strength);
                    if (enemy.hp<=0) {
                        enemy.hp = 0;
                    }
                    System.out.format("%s has %d health!\n", enemy.name, enemy.hp);
                }
                else if (attack.equals("two")) {
                    usedAttack = hero.Attacks.get(1);
                    enemy.hp-= usedAttack.strenght*hero.strength;
                    System.out.format("%s dealt %d damage!\n", hero.name, 1*hero.strength);
                    if (enemy.hp<=0) {
                        enemy.hp = 0;
                    }
                    System.out.format("%s has %d health!\n", enemy.name, enemy.hp);
                }
                else if (attack.equals("three")) {
                    usedAttack = hero.Attacks.get(2);
                    supposedHeal = usedAttack.strenght*hero.strength;
                    hero.Heal(supposedHeal);
                    System.out.format("%s healed %d!\n", hero.name, supposedHeal);
                }
                else if (attack.equals("four")) {
                    usedAttack = hero.Attacks.get(3);
                    supposedHeal = usedAttack.strenght*hero.strength;
                    carriedHp = supposedHeal/2;
                    hero.Heal(supposedHeal);
                    System.out.format("%s healed %d!\n", hero.name, supposedHeal);
                }

                if (carriedHp>0) {
                    System.out.format("%s healed %d from last turn!\n", hero.name, carriedHp);
                    carriedHp = 0;
                    
                }
                
                

            }
            else if(command.equals("block"))
            {
                dmgRed=1*hero.strength;
            }
            


            if (enemy.hp<=0) {
                enemy.hp = 0;
                int wonXp = GetExperience();
                hero.experience+=wonXp;
                hero.UpdateExperience();
                System.out.format("%s killed %s\n", hero.name, enemy.name);
                System.out.format("Gained %d experience\n",wonXp);
                enemy.IsAlive = false;
                
                
                System.out.print("Congrats!\n");
                
                
                
            }
            else{
                Attack enemyUsedAttack = GetAttack(enemy);
                int damageTaken = (enemy.strenght*enemyUsedAttack.strenght)-dmgRed;
                hero.currentHp -= damageTaken;
                System.out.format("%s dealt %d damage!\n", enemy.name, damageTaken);
                dmgRed = 0;

                if(hero.currentHp<0)
                {
                    hero.currentHp = 0;
                    System.out.format("%s died!\n", hero.name, damageTaken);

                    return false;
                }
                else
                {
                    System.out.format("%s has %d hp left!\n", hero.name, hero.currentHp);
                }

                
            }

            
        }

        return true;

    }

    private static Attack GetAttack(Enemy enemy){

        int maximum = enemy.Attacks.size();
        int minimum = 0;
        int attackNum = (int)(Math.random()*(maximum - minimum)) + minimum;
        return enemy.Attacks.get(attackNum);

    }
    private static int GetExperience(){

        int maximum = 10;
        int minimum = 1;
        int attackNum = (int)(Math.random()*(maximum - minimum)) + minimum;
        return attackNum;

    }
}
