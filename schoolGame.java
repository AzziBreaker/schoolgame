import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.*;

import classes.*;
import classes.Hero;
import frame.GameFrame;

public class schoolGame {

    static void MakeFrame(GameFrame frame,Map map) {
        
        
    }
    static void ClearConsole() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
  
    static void shouldIMove(char commandGo, Map map) {
        //if (commandDo.equals("move") || commandDo.equals("go")) {
            map.hero.Move(commandGo, map);
       // }
    }

    static void shouldIEnd(String commandDo, boolean progress) {
        if (commandDo.equals("end")) {
            progress = false;
        }
    }

    //TODO: Make attacks
    static List < Attack > makeAttacks() {
        List < Attack > attacks = new ArrayList < Attack > ();

        return attacks;
    }
    public static void main(String args[]) {

        ClearConsole();
        Scanner scanner = new Scanner(System.in); 
        Hero hero;
        Map map;
        Boolean progress = true;
        try {
            System.out.println("Well its in");
            FileInputStream fis = new FileInputStream("infoFiles/game.bin");
            System.out.println("Shit kinda works");
            ObjectInputStream ois = new ObjectInputStream(fis);
            map = (Map)ois.readObject();
            hero = map.hero;
            map.makeMap();
            ois.close();
            fis.close();

        } catch (IOException | ClassNotFoundException e) {
            
            hero = new Hero("Kris", "male");
            map = new Map(1,hero);
            map.makeMap();
            
            ItemWeapon sword = new ItemWeapon("Sword", "Wooden", 1);
            ItemArmor helmet = new ItemArmor("Helmet", "Wooden", 1);
            ItemArmor chestplate = new ItemArmor("Chestplate", "Wooden", 1);
            ItemArmor leggings = new ItemArmor("Leggings", "Wooden", 1);
            ItemArmor boots = new ItemArmor("Boots", "Wooden", 1);
            hero.EquipWeapon(sword);
            hero.EquipArmor(helmet);
            hero.EquipArmor(chestplate);
            hero.EquipArmor(leggings);
            hero.EquipArmor(boots);
            
        }

        GameFrame frame = new GameFrame(map);
        
        
       
        while (progress) {

            frame.repaint();
            //ClearConsole();
            map.ShowRoom();
            //String[] commandDo = scanner.nextLine().trim().split("\\s+");
            String command = scanner.nextLine();
            

            if (command.toLowerCase().equals("stats")) {
                hero.Stats();
                command = scanner.nextLine();
                if (command.toLowerCase().equals("back")) {
                    continue;
                }
            }
            else if(command.toLowerCase().equals("who")) {
                map.whoLeft();
                command = scanner.nextLine();
                if (command.toLowerCase().equals("back")) {
                    continue;
                }
            }
            else if(command.toLowerCase().equals("end")){
                try {
                   FileOutputStream fos = new FileOutputStream("infoFiles/game.bin");
                   ObjectOutputStream oos = new ObjectOutputStream(fos);
                   oos.writeObject(map);
                   oos.close();
                   fos.close(); 

                } catch (Exception e) {
                    
                }
                progress = false;
                scanner.close();
                System.exit(0);
            }
            else {
                try {
                    char go = command.toLowerCase().charAt(0);
                    shouldIMove(go,map);
                } catch (StringIndexOutOfBoundsException e) {
                    System.out.print("Wrong input!");
                }   
            }
        }
    }
}