package frame;

import classes.*;
import java.awt.*;
import java.awt.TextArea;
import java.awt.TextField;

import javax.print.attribute.standard.DialogOwner;

import java.awt.Button;


public class GameFrame extends Frame {

    public Map map;
    public Label label;
    public TextField textField;
    public TextArea textArea;
    public Button left;
    public Button right;
    public Button up;
    public Button down;
    public Button attackOne;
    public Button attackTwo;
    public Button attackThree;
    public Button attackFour;
    public GameMapPanel mapPanel;
    public GameDialoguePanel dialoguePanel;

    public GameFrame(Map map) {
        this.map = map;
        this.setBackground(Color.pink);
        this.initPanels(map);
        this.initListiners();
        this.initButtons();
        this.setLayout(null);
        this.initFrame(map);
        }

    private void initButtons() {
        this.left = new Button();
        this.setUpMoveButton(left,"←", 20, 90 + 70);
        this.right = new Button();
        this.setUpMoveButton(right,"→", 20+240,90+70);
        this.up = new Button();
        this.setUpMoveButton(up,"↑", 20+120,40);
        this.down = new Button();
        this.setUpMoveButton(down,"↓", 20+120,280);

        this.attackOne = new Button();
        this.setUpAttackButton( attackOne, "Attack one", 50, 330);
        this.attackTwo = new Button();
        this.setUpAttackButton( attackTwo, "Attack two", 50+(72*1),330);
        this.attackThree = new Button();
        this.setUpAttackButton( attackThree, "Attack three", 50+(72*2),330);
        this.attackFour = new Button();
        this.setUpAttackButton( attackFour, "Attack four", 50+(72*3),330);
    }

    private void setUpAttackButton(Button button, String name, int x, int y) {
        button.setLabel(name);
        GameAttackButtonListener attackListener = new GameAttackButtonListener(this, button);
        button.setLocation(x,y);
        button.setSize(72,40);
        button.setVisible(true);
        button.addActionListener(attackListener);
        this.add(button);
    }

    private void setUpMoveButton(Button button, String icon, int x, int y) {
        button.setLabel(icon);
        GameMoveButtonListener moveButtonListener = new GameMoveButtonListener(this,button);
        button.setLocation(x,y);
        button.setSize(30,30);
        button.setVisible(false);
        button.addActionListener(moveButtonListener);
        this.add(button);
    }

    private void initListiners() {
        GameWindowListener gwl = new GameWindowListener();
        addWindowListener(gwl);
        GameKeyListener gkl = new GameKeyListener(this);
        mapPanel.addKeyListener(gkl);
        GameMouseListener gml = new GameMouseListener(this);
        mapPanel.addMouseListener(gml);
    }

    private void initPanels(Map map) {
        mapPanel = new GameMapPanel(this);
        mapPanel.setLocation(50,70);
        mapPanel.setSize(map.GetRoom().Size*30+1,map.GetRoom().Size*30+1);
        mapPanel.setBackground(Color.white);
        this.add(mapPanel);

        dialoguePanel = new GameDialoguePanel(this);
        dialoguePanel.setLocation(320,70);
        dialoguePanel.setSize(150,240);
        dialoguePanel.setBackground(Color.white);
        this.add(dialoguePanel);

    }

    private void initFrame(Map map) {
        this.setSize(800,500);
        this.setVisible(true);
        this.setResizable(false);
        this.map.frame = this;
    }


    public void DisplayButtons() {
        if (map.hero.RoomPositionX==0) {
            this.up.setVisible(true);
            this.down.setVisible(false);
            this.left.setVisible(false);
            this.right.setVisible(false);
        }
        else if(map.hero.RoomPositionX==map.GetRoom().Size-1){
            this.up.setVisible(false);
            this.down.setVisible(true);
            this.left.setVisible(false);
            this.right.setVisible(false);
        }
        else if(map.hero.RoomPositionY==0){
            this.up.setVisible(false);
            this.down.setVisible(false);
            this.left.setVisible(true);
            this.right.setVisible(false);
        }
        else if(map.hero.RoomPositionY==map.GetRoom().Size-1){
            this.up.setVisible(false);
            this.down.setVisible(false);
            this.left.setVisible(false);
            this.right.setVisible(true);
        }
        else
        {
            this.up.setVisible(false);
            this.down.setVisible(false);
            this.left.setVisible(false);
            this.right.setVisible(false);
        }
    }

    public void DisplayAttacks() {

    }

    public static void ShowStats() {
        
        
    }

}
