package frame;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import classes.*;

public class GameMouseListener implements MouseListener {
    GameFrame fr;
    GameMapPanel frame;
    Map map;
    Hero hero;
    

    public GameMouseListener(GameFrame f) {
            this.fr = f;
            this.frame = f.mapPanel;
            map = frame.map;
            hero = frame.map.hero;
            
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        int y = e.getX();
        int x = e.getY();
        int squareX = (x)/30;
        int squareY = (y)/30;

        if ((squareX<map.GetRoom().Size && squareX>=0 ) && (squareY>=0 && squareY<map.GetRoom().Size)) {
             hero.MoveWithMouse(map, squareX, squareY);
        }
		else if((squareX==hero.RoomPositionX ) && (squareY==hero.RoomPositionY)){
             hero.ShowStats();
        }
        
        fr.DisplayButtons();
        map.ShowRoom();
		frame.repaint();


    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
        
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        // TODO Auto-generated method stub
        
    }
    
}
