package frame;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import classes.*;

public class GameMapPanel extends Panel{
    GameFrame fr;
    GameMapPanel frame;
    Map map;
    public GameMapPanel(GameFrame frame) {
        this.fr = frame;
        this.frame = frame.mapPanel;
        this.map = frame.map;
    }

    private void fillRectangle(Graphics graphics, int x, int y, Color c) {
        graphics.setColor(c);
        graphics.fillRect((x*30), (y*30), 30, 30);
    }

    private void DrawSprite(Graphics graphics, int x, int y, String path) {

        BufferedImage img = null;
        File f = new File(path);
        try {
            img = ImageIO.read(f);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        graphics.drawImage(img, x*30, y*30, null);
    }

    public void paint(Graphics graphics){

        Room room = map.GetRoom();
        int posY = map.hero.RoomPositionX;
        int posX = map.hero.RoomPositionY;
        
        

        for (int y = 0; y < room.Size ; y++) {
            for (int x = 0; x < room.Size; x++) {


                if (!map.GetRoom().IsRoomMade) {
                    graphics.setColor(Color.black);
                    graphics.drawRect((x*30), (y*30), 30, 30);
                    int block = GetBlock();
                    
                    
                    if(room.layout[y][x]==' ')
                    {
                        String path = String.format("grass%d.png", block);
                        map.GetRoom().paths[y][x] = path;
                        DrawSprite(graphics, x, y, path);
                    }
                    else if (room.layout[y][x]=='e') {
                        map.GetRoom().paths[y][x] = "enemy.png";
                        DrawSprite(graphics, x, y, "enemy.png");
                    }
                    else if (room.layout[y][x]=='─' || room.layout[y][x]=='│' ||room.layout[y][x]=='┐' ||room.layout[y][x]=='┌' ||room.layout[y][x]=='└' ||room.layout[y][x]=='┘' ) {
                        map.GetRoom().paths[y][x] = "wall.png";
                        DrawSprite(graphics, x, y, "wall.png");
                    }
                    else if (room.layout[y][x]=='c') {
                        map.GetRoom().paths[y][x] = "chest.png";
                        DrawSprite(graphics, x, y, "chest.png");
                    }

                    if (x==posX && y==posY) {
                        String path = String.format("grass%d.png", block);
                        map.GetRoom().paths[y][x] = path;
                        DrawSprite(graphics, x, y, path);
                        DrawSprite(graphics, x, y, "hero.png");
                    }
                    if (!map.GetRoom().paths[6][6].equals("")) {
                        map.GetRoom().IsRoomMade = true;
                    }
                    
                }
                else
                {
                    DrawSprite(graphics, x, y, map.GetRoom().paths[y][x]);
                    if (x==posX && y==posY) {
                        DrawSprite(graphics, x, y, "hero.png");
                    }
                }
                

                
            }
        }

    }

    private int GetBlock(){

        int maximum = 5;
        int minimum = 1;
        int blockNum = (int)(Math.random()*(maximum - minimum)) + minimum;
        return blockNum;

    }
}
