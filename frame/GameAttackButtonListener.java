package frame;

import java.awt.event.*;

import classes.Hero;
import classes.Map;

import java.awt.Button;

public class GameAttackButtonListener implements ActionListener{
    GameMapPanel frame;
    GameFrame fr;
    Hero hero;
    Map map;
    Button button;

    public GameAttackButtonListener(GameFrame frame, Button button) {
        this.frame = frame.mapPanel;
        this.fr = frame;
        this.hero = frame.map.hero;
        this.button = button;
        this.map = frame.map;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (button.getLabel().equals("Attack one")) {
            hero.Move('a', map);
        }
        else if (button.getLabel().equals("Attack two")) {
            hero.Move('w', map);
        }
        else if (button.getLabel().equals("Attack three")) {
            hero.Move('d', map);
        }
        else if (button.getLabel().equals("Attack four")) {
            hero.Move('s', map);
        }
        
        fr.DisplayButtons();
        frame.repaint();
    }
    
}
