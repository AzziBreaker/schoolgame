package frame;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import classes.*;
import classes.Hero;

public class GameKeyListener implements KeyListener {
    GameFrame fr;
    GameMapPanel frame;
    Map map;
    Hero hero;

    public GameKeyListener(GameFrame frame) {
        this.fr = frame;
        this.frame = frame.mapPanel;
        map = frame.map;
        hero = map.hero;
    }

    @Override
    public void keyPressed(KeyEvent e) {

       char c = e.getKeyChar();
       
       hero.Move(c, map);
       frame.repaint();
       System.out.println(e.getKeyChar());

       fr.DisplayButtons();
    
    frame.repaint();
    map.ShowRoom();
        
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void keyTyped(KeyEvent arg0) {
        // TODO Auto-generated method stub
        
    }
    
}
