package frame;

import java.awt.event.*;

import classes.Hero;
import classes.Map;

import java.awt.Button;

public class GameMoveButtonListener implements ActionListener{
    GameMapPanel frame;
    GameFrame fr;
    Hero hero;
    Map map;
    Button button;

    public GameMoveButtonListener(GameFrame frame, Button button) {
        this.frame = frame.mapPanel;
        this.fr = frame;
        this.hero = frame.map.hero;
        this.button = button;
        this.map = frame.map;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (button.getLabel().equals("←")) {
            hero.Move('a', map);
        }
        else if (button.getLabel().equals("↑")) {
            hero.Move('w', map);
        }
        else if (button.getLabel().equals("→")) {
            hero.Move('d', map);
        }
        else if (button.getLabel().equals("↓")) {
            hero.Move('s', map);
        }
        System.out.format("Hero is in room[%d][%d]\n",hero.RoomX,hero.RoomY);
        fr.DisplayButtons();
        frame.repaint();
    }
    
}
